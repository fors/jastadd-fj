package org.jastadd.fj.compiler;

import org.jastadd.fj.compiler.FJScanner.ScannerError;

import java.io.*;
import java.util.*;
import beaver.Parser;

public class Compiler {
	public static Object DrAST_root_node;
	public static void main(String args[]) {
		if (args.length == 1) {
			Program p = parseFile(args[0]);
			DrAST_root_node = p;

			System.out.println(p);

			for (ClassDecl cd: p.getClassDecls()) {
				System.out.println(cd.name() + ": "
					+ (cd.isOK() ? "OK" : "NOT OK" ));
			}
			if (p.hasExpr()) {
				System.out.println("Type of expression: "
					+ p.getExpr().type().name());
			}
		} else {
			System.err.println("Error! Specify filename for fj program");
			System.exit(1);
		}
	}

	private static Program parseFile(String file) {
		FileReader reader = null;
		try {
			reader = new FileReader(file);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
		return parse(reader, file);
	}

	private static Program parse(Reader reader, String file) {
		FJScanner scanner = new FJScanner(reader);
		FJParser parser = new FJParser();

		try {
			return (Program) parser.parse(scanner);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		} catch (Parser.Exception e) {
			System.err.println("Parse error in file " + file);
			System.err.println(e.getMessage());
			System.exit(1);
		} catch (ScannerError e) {
			System.err.println("Scanner error in file " + file);
			System.err.println(e.getMessage());
			System.exit(1);
		}
		return null;
	}

}
