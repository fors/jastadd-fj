package org.jastadd.fj.tests.testsuite;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.util.concurrent.TimeUnit;

import org.jastadd.fj.compiler.*;

abstract public class TestSuite {
	protected static final String TEST_FILES_PATH = "testfiles/";

	protected static void checkErrors(String filename) {
		Program p = parseProgramFile(filename + ".fj");
		String expectedErrors = readTestFile(filename + ".expected");
		String actualErrors = getOutput(p);
		assertEquals(expectedErrors.trim(), actualErrors.trim());
	}

	protected static Program parseProgramFile(String filename) {
		return parseProgramFile(new File(TEST_FILES_PATH, filename));
	}

	protected static Program parseProgramFile(File file) {
		try {
			Reader r = new FileReader(file);
			return parseProgram(r, "Error when parsing the file " + file.getAbsolutePath());
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
			return null;
		}
	}

	protected static Program parseProgram(Reader reader, String errorMessage) {
		PrintStream err = System.err;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(os);

		try {
			System.setErr(ps);

			Program p = parse(reader);
			if (os.size() > 0) {
				// The parser should not recover from anything, such errors
				// should not be put in a test case (otherwise it should be
				// stated explicitly).
				fail("Parser recovery:\n" + os.toString());
			}
			return p;
		} catch (Exception e) {
			fail(errorMessage + ":\n " + e.getMessage() + "\n" + os.toString());
			return null; // This line is required to remove compile errors...
		} finally {
			System.setErr(err);
		}
	}

	protected static String getOutput(Program p) {
		StringBuilder sb = new StringBuilder();
		for (ClassDecl cd: p.getClassDecls()) {
			sb.append(cd.name() + ": ");
			sb.append(cd.isOK() ? "OK" : "NOT OK");
			sb.append("\n");
		}
		if (p.hasExpr()) {
			sb.append("Type of expression: ");
			sb.append(p.getExpr().type().name());
			sb.append("\n");
		}
		return sb.toString();
	}


	//-----------------------------------------------------------
	//
	// Helper methods
	//
	//-----------------------------------------------------------

	protected static String readTestFile(String filename) {
		return readFile(TEST_FILES_PATH + filename);
	}
	protected static String readFile(String filename) {
		StringBuilder sb = new StringBuilder();
		BufferedReader br = null;
		try {
			File file = new File(filename);
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
			  sb.append(line).append("\n");
			}
		} catch (IOException e) {
			fail(e.getMessage());
		} finally {
			if (br != null) {
				{ try { br.close();} catch (IOException ignored) { } }
			}
		}
		return sb.toString();
	}


	protected static String inputStreamToString(InputStream is) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
		return sb.toString();
	}

	private static Program parse(Reader reader) throws IOException,
			beaver.Parser.Exception {
		FJScanner scanner = new FJScanner(reader);
		FJParser parser = new FJParser();
		return (Program) parser.parse(scanner);
	}

}
